Introduction
------------
Poormansmongo provides an alternative field storage backend.
All poormansmongo fields are saved as a serialized object on the entity itself.
You will loose the ability to query on these fields, but you will gain
performance in entity_load, and you can regain the ability to query
by indexing them with Solr.

Known issues
------------
Currently there is no decent cleanup for poormansmongo fields that are removed.

Installation
------------

Enable the module under admin/modules

Requirements
------------
None
