<?php

/**
 * @file
 * Entity related functions for poormansmongo module.
 */

/**
 * Returns all entity info relevant for poormansmongo functionality.
 *
 * @see poormansmongo_entity_info()
 * @see poormansmongo_schema_alter()
 * @see poormansmongo_install()
 * @see poormansmongo_uninstall()
 */
function poormansmongo_get_core_entity_info() {
  $entities_info = entity_get_info();
  foreach ($entities_info as $entity_type => &$entity_info) {
    if (isset($entity_info['fieldable']) && $entity_info['fieldable']) {
      $entity_info['entity keys']['poormansmongo'] = 'poormansmongo';
      if (!empty($entity_info['revision table'])) {
        $entity_info['entity keys']['revision poormansmongo'] = 'poormansmongo';
      }
    }
    else {
      unset($entities_info[$entity_type]);
    }
  }

  return $entities_info;
}

/**
 * Implements of hook_entity_presave().
 */
function poormansmongo_entity_presave($entity, $entity_type) {
  $entity_info = entity_get_info($entity_type);
  $primary_key = $entity_info['entity keys']['id'];
  $entity->poormansmongo = array();
  foreach ($entity as $fieldname => &$value) {
    if (substr_count($fieldname, 'field_')) {
      $info = field_info_field($fieldname);
      if ($info['storage']['type'] == 'poormansmongo_field_storage') {
        _field_invoke_multiple('load', $entity_type, array($entity->{$primary_key} => $entity));
        $items = field_get_items($entity_type, $entity, $info['field_name']);
        if (!empty($items)) {
          $entity->poormansmongo[$info['field_name']] = $entity->{$info['field_name']};
        }
      }
    }
  }
}

/**
 * Implements of hook_entity_load().
 */
function poormansmongo_entity_load($entities, $type) {
  foreach ($entities as &$entity) {
    if (isset($entity->poormansmongo)) {
      $blob = unserialize($entity->poormansmongo);
      if (is_array($blob)) {
        foreach ($blob as $name => $value) {
          $info = field_info_field($name);
          if (isset($blob[$info['field_name']])) {
            $entity->{$info['field_name']} = $blob[$info['field_name']];
          }
        }
      }
    }
  }
}
